<?php

/**
 * @file
 * Custom content type.
 *
 * This content type is nothing more than a title and a body that is entered
 * by the user and run through standard filters. The information is stored
 * right in the config, so each custom content is unique.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'key' => 'file',
  'single' => TRUE,
  'title' => t('New file'),
  'icon' => 'icon_file.png',
  'description' => t('Create a file.'),
  // Make this a top level category so it appears higher in UIs that support
  // that.
  'top level' => TRUE,
  'category' => t('Custom'),
  // render callback is automatically deduced:
  'render callback' => 'ctoolscustomplugins_file_content_type_render',
  'js' => array('misc/autocomplete.js', 'misc/textarea.js', 'misc/collapse.js'),
  'all contexts' => TRUE,
  'defaults' => array(
    'admin_title' => '',
    'title' => '',
    'file_upload' => '',
    'file_title' => '',
    'file_description' => '',
    'file_css' => '',
  ),
);

/**
 * Output function for the 'custom' content type. Outputs a custom
 * based on the module and delta supplied in the configuration.
 */
function ctoolscustomplugins_file_content_type_render($subtype, $conf, $args, $contexts) {
  static $delta = 0;

  $block          = new stdClass();
  $block->subtype = ++$delta;
  $block->title   = filter_xss_admin($conf['title']);
  $block->content = '';
  $block->content .= theme_panel_file(filter_xss_admin($conf['file_title']), $conf['files'], $conf['file_css'], $conf['file_description']);
  return $block;
}

function ctoolscustomplugins_file_content_type_admin_info($subtype, $conf) {
  $block = new stdClass();
  $block->title   = filter_xss_admin($conf['title']);
  $block->content = '';
  $block->content .= theme_panel_file(filter_xss_admin($conf['file_title']), $conf['files'], $conf['file_css'], $conf['file_description']);
  
  return $block;
}

/**
 * Callback to provide the administrative title of the custom content.
 */
function ctoolscustomplugins_file_content_type_admin_title($subtype, $conf) {
  $output = t('Custom');
  $title = !empty($conf['admin_title']) ? $conf['admin_title'] : $conf['title'];
  if ($title) {
    $output = t('Custom: @title', array('@title' => $title));
  }
  return $output;
}

/**
 * Returns an edit form for the custom type.
 */
function ctoolscustomplugins_file_content_type_edit_form(&$form, &$form_state) {
  $conf = $form_state['conf'];
  $form['admin_title'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($conf['admin_title']) ? $conf['admin_title'] : '',
    '#title' => t('Administrative title'),
    '#description' => t('This title will be used administratively to identify this pane. If blank, the regular title will be used.'),
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#default_value' => $conf['title'],
    '#title' => t('Title'),
    '#description' => t("This will be used as the Pane title"),
  );
  
  $form['#attributes']['enctype'] = 'multipart/form-data';
  
  $form['file'] = array(
    '#prefix' => '<div class="file-field-wrapper">',
    '#suffix' => '</div>',
  );
  
  $form['file']['preview'] = array(
    '#type' => 'item',
    '#after_build' => array('ctoolscustomplugins_file_preview'),
  );
  
  $form['file']['file_upload'] = array(
    '#title' => t('Upload file'),
    '#type'  => 'file',
  );
  
  $form['file']['file_title'] = array(
    '#type' => 'textfield',
    '#default_value' => $conf['file_title'],
    '#title' => t('File Title'),
    '#description' => t("This will be the name that replaces the filename. If empty, the filename will be used"),
  );
  
  $form['file']['file_description'] = array(
    '#type' => 'textfield',
    '#default_value' => $conf['file_description'],
    '#title' => t("File description"),
    '#description' => t("The file description"),
  );
  
  $form['file']['file_css'] = array(
    '#type' => 'textfield',
    '#default_value' => $conf['file_css'],
    '#title' => t("CSS Class"),
    '#description' => t("The CSS Class to apply to the link"),
  );

  return $form;
}

/**
 * We submit the form and ctools saves the data
 * TODO: Add some validators to the file upload
 */
function ctoolscustomplugins_file_content_type_edit_form_submit(&$form, &$form_state) {
  $validators = array();  
  if (user_access('upload files in panels') && ($file = file_save_upload('file_upload', $validators, file_directory_path(), FILE_EXISTS_RENAME))) {
    $file->description = $file->filename;
    $file->weight = 0;
    $file->new = TRUE;
    $form_state['conf']['file'] = (array)$file;
  }
  
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}


/**
 * If we already have an image, we display it
 */
function ctoolscustomplugins_file_preview($form, &$form_state) {
  if ($form_state['conf']['file']) {
    $file = (object)($form_state['conf']['file']);
    $form['#title'] = t("File Details");
    $form['#value'] = theme_panel_file_preview($file);
    $form['#description'] = t("If you want to remove this file just upload a new one");
  }
  return $form;
}
