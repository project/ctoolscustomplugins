<?php
/**
 * @file
 * Custom content type.
 *
 * This content type lets you add an image that can also be a link.
 * The image will not be saved as a node and imagecache is required to
 * display the image.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'key' => 'image',
  'single' => TRUE,
  'title' => t('New image'),
  'icon' => 'icon_image.png',
  'description' => t('Create an image.'),
  // Make this a top level category so it appears higher in UIs that support
  // that.
  'top level' => TRUE,
  'category' => t('Custom'),
  // render callback is automatically deduced:
  'render callback' => 'ctoolscustomplugins_image_content_type_render',
  'js' => array('misc/autocomplete.js', 'misc/textarea.js', 'misc/collapse.js'),
  'all contexts' => TRUE,
  'defaults' => array(
    'admin_title' => '',
    'title' => '',
    'image_upload' => '',
    'image_description' => '',
    'image_caption' => '',
    'image_css' => '',
    'image_preset' => '',
    'link_uri' => '',
    'link_rel' => '',
    'link_css' => '',
    'link_target' => '',
   ),
);

/**
 * Output function for the 'custom' content type. Outputs a custom
 * based on the module and delta supplied in the configuration.
 */
function ctoolscustomplugins_image_content_type_render($subtype, $conf, $args, $contexts) {
  static $delta = 0;

  $block          = new stdClass();
  $block->subtype = ++$delta;
  $block->title   = filter_xss_admin($conf['title']);
  $block->content = '';
  $preset = imagecache_preset($conf['image_preset'], $reset = FALSE);
  if($conf['link_uri']) {
    if($conf['link_target']) {
      $target = '_blank';
    }
    else {
      $target = '';
    }
    $block->content .= l(theme_imagecache($preset['presetname'], $conf['image']['filename']), $conf['link_uri'], $options = array('html' => TRUE, 'attributes' => array('class' => $conf['link_css'], 'rel' => $conf['link_rel'], 'target' => $target)));
  }
  else {
    $block->content .= theme_imagecache($preset['presetname'], $conf['image']['filename'], $conf['image_description'], $conf['image_description'], $attributes = array('class' => $conf['image_css']));
  }
  
  if($conf['image_caption']) {
    $block->content .= '<span class="image-caption">' . t($conf['image_caption']) . '</span>';
  }
  
  return $block;
}

/**
 * Callback to provide the administrative title of the custom content.
 */
function ctoolscustomplugins_image_content_type_admin_title($subtype, $conf) {
  $output = t('Custom');
  $title = !empty($conf['admin_title']) ? $conf['admin_title'] : $conf['title'];
  if ($title) {
    $output = t('Custom: @title', array('@title' => $title));
  }
  return $output;
}

function ctoolscustomplugins_image_content_type_admin_info($subtype, $conf) {
  $block = new stdClass();
  $block->title   = filter_xss_admin($conf['title']);
  $block->content = '';
  $preset = imagecache_preset($conf['image_preset'], $reset = FALSE);
  // If we have a URI the image is a link
  if($conf['link_uri']) {
    if($conf['link_target']) {
      $target = '_blank';
    }
    else {
      $target = '';
    }
    $block->content .= l(theme_imagecache($preset['presetname'], $conf['image']['filename']), $conf['link_uri'], $options = array('html' => TRUE, 'attributes' => array('class' => $conf['link_css'], 'rel' => $conf['link_rel'], 'target' => $target)));
  }
  else {
    $block->content .= theme_imagecache($preset['presetname'], $conf['image']['filename'], $conf['image_description'], $conf['image_description'], $attributes = array('class' => $conf['image_css']));
  }
  
  if($conf['image_caption']) {
    $block->content .= '<span class="image-caption">' . t($conf['image_caption']) . '</span>';
  }
  
  return $block;
}

/**
 * Returns an edit form for the custom type.
 */
function ctoolscustomplugins_image_content_type_edit_form(&$form, &$form_state) {
  $conf = $form_state['conf'];
  $form['admin_title'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($conf['admin_title']) ? $conf['admin_title'] : '',
    '#title' => t('Administrative title'),
    '#description' => t('This title will be used administratively to identify this pane. If blank, the regular title will be used.'),
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#default_value' => $conf['title'],
    '#title' => t('Title'),
    '#description' => t("This will be used as the Pane title"),
  );
  
  if(module_exists('imagecache')) {
    $form['#attributes']['enctype'] = 'multipart/form-data';
    
    $form['image'] = array(
      '#prefix' => '<div class="image-field-wrapper">',
      '#suffix' => '</div>',
    );
    $form['image']['preview'] = array(
      '#type' => 'item',
      '#after_build' => array('ctoolscustomplugins_image_preview'),
    );
    $form['image']['image_upload'] = array(
      '#title' => t('Upload image'),
      '#type'  => 'file',
    );
    
    $presets = array();
    $presets[] = '';
    foreach (imagecache_presets() as $key => $preset) {
      $presets[$key] = check_plain($preset['presetname']);
    }
  
    $form['image']['image_preset'] = array(
      '#type' => 'select',
      '#title' => t("Imagecache Preset"),
      '#description' => t('Select an imagecache preset'),
      '#default_value' => $conf['image_preset'],
      '#options' => $presets,
    );
    $form['image']['image_description'] = array(
      '#type' => 'textfield',
      '#default_value' => $conf['image_description'],
      '#title' => t("Image description"),
      '#description' => t("The image description will be used as an image caption and as the image title"),
    );
    $form['image']['image_caption'] = array(
      '#type' => 'textfield',
      '#default_value' => $conf['image_caption'],
      '#title' => t("Image caption"),
      '#description' => t("This will be used as an image caption"),
    );
    $form['image']['image_css'] = array(
      '#type' => 'textfield',
      '#default_value' => $conf['image_css'],
      '#title' => t("CSS Class"),
      '#description' => t("The CSS class to apply to the image"),
    );
    
    // Sometimes we want the image to become a link, we provide this functionality   
    $form['link'] = array(
      '#type' => 'fieldset',
      '#title' => t("Link"),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
    );
    $form['link']['link_uri'] = array(
      '#type' => 'textfield',
      '#title' => t("Link URI"),
      '#default_value' => $conf['link_uri'],
      '#description' => t("If you want to make this image a link, please enter a URI"),
    );
    $form['link']['link_target'] = array(
      '#type' => 'checkbox',
      '#title' => t("Open in a new window"),
      '#default_value' => $conf['link_target'],
    );
    $form['link']['link_css'] = array(
      '#type' => 'textfield',
      '#title' => t("CSS class"),
      '#default_value' => $conf['link_css'],
      '#description' => t("CSS class of this link")
    );
    $form['link']['link_rel'] = array(
      '#type' => 'textfield',
      '#title' => t("Rel"),
      '#default_value' => $conf['link_rel'],
      '#description' => t("The rel attribute specifies the relationship between the current document and the linked document.")
    );

  }
  else {
    drupal_set_message("Please install Imagecache");
  }
  
  return $form;
}

/**
 * We tell ctools to save the image array
 */
function ctoolscustomplugins_image_content_type_edit_form_submit(&$form, &$form_state) {
  // Validators for file_save_upload().
  $validators = array(
    'file_validate_is_image' => array(),
  );
  if (user_access('upload images in panels') && ($image = file_save_upload('image_upload', $validators, file_directory_path(), FILE_EXISTS_RENAME))) {
    $image->description = $image->filename;
    $image->weight = 0;
    $image->new = TRUE;
    $form_state['conf']['image'] = (array)$image;
  }
  
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * If we already have an image, we display it
 */
function ctoolscustomplugins_image_preview($form, &$form_state) {
  if ($form_state['conf']['image']) {
    $image = (object)($form_state['conf']['image']);
    $form['#title'] = t("Preview");
    $form['#value'] = theme_panel_image_preview($image->filepath);
    $form['#description'] = t("If you want to remove this image just upload a new one");
  }
  return $form;
}