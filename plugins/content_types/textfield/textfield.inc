<?php

/**
 * @file
 * Custom content type.
 *
 * This content type is nothing more than a title and a body that is entered
 * by the user and run through standard filters. The information is stored
 * right in the config, so each custom content is unique.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'key' => 'textfield',
  'single' => TRUE,
  'title' => t('New text field'),
  'icon' => 'icon_textfield.png',
  'description' => t('Create a text field.'),
  // Make this a top level category so it appears higher in UIs that support
  // that.
  'top level' => TRUE,
  'category' => t('Custom'),
  'defaults' => array(
    'admin_title' => '',
    'textfield' => '',
    'textfield_css' => '',
  ),
  // render callback is automatically deduced:
  'render callback' => 'ctoolscustomplugins_textfield_content_type_render',
  'js' => array('misc/autocomplete.js', 'misc/textarea.js', 'misc/collapse.js'),
  'all contexts' => TRUE,
);

/**
 * Output function for the 'custom' content type. Outputs a custom
 * based on the module and delta supplied in the configuration.
 */
function ctoolscustomplugins_textfield_content_type_render($subtype, $conf, $args, $contexts) {
  static $delta = 0;

  $block          = new stdClass();
  $block->subtype = ++$delta;
  $block->title   = '';
  $block->content = '';
  if($conf['textfield_css']) { 
    $block->content .= '<span class="'. $conf['textfield_css'] .'">' . t($conf['textfield']) . '</span>';
  }
  else {
    $block->content .= t($conf['textfield']);
  }

  
  return $block;
}

/**
 * Callback to provide administrative info. In this case we'll render the
 * content as long as it's not PHP, which is too risky to render here.
 */
function ctoolscustomplugins_textfield_content_type_admin_info($subtype, $conf) {
  $block = new stdClass();
  $block->title = '';
  $block->content = '';
  if($conf['textfield_css']) { 
    $block->content .= '<span class="'. $conf['textfield_css'] .'">' . t($conf['textfield']) . '</span>';
  }
  else {
    $block->content .= t($conf['textfield']);
  }
  
  return $block;
}

/**
 * Callback to provide the administrative title of the custom content.
 */
function ctoolscustomplugins_textfield_content_type_admin_title($subtype, $conf) {
  $output = t('Custom');
  $title = !empty($conf['admin_title']) ? $conf['admin_title'] : $conf['textfield'];
  if ($title) {
    $output = t('Custom: @title', array('@title' => $title));
  }
  return $output;
}

/**
 * Returns an edit form for the custom type.
 */
function ctoolscustomplugins_textfield_content_type_edit_form(&$form, &$form_state) {
  $conf = $form_state['conf'];
  $form['admin_title'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($conf['admin_title']) ? $conf['admin_title'] : '',
    '#title' => t('Administrative title'),
    '#description' => t('This title will be used administratively to identify this pane. If blank, the regular title will be used.'),
  );
  
  $form['textfield'] = array(
    '#type' => 'textfield',
    '#default_value' => $conf['textfield'],
    '#title' => t('Text'),
  );
  
  $form['textfield_css'] = array(
    '#type' => 'textfield',
    '#default_value' => $conf['textfield_css'],
    '#title' => t('Text CSS Class'),
    '#description' => t("Add a CSS class to control how this text field looks"),
  );

  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function ctoolscustomplugins_textfield_content_type_edit_form_submit(&$form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }  
}
